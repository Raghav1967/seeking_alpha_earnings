import scrapy
import logging

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    base_url = 'https://seekingalpha.com'
    start_urls = ['https://seekingalpha.com/']

    def start_requests(self):
        for i in range(1, 1000):

            url = 'https://seekingalpha.com/earnings/earnings-news/' + str(i)

            yield scrapy.Request(url, self.parse)

    def parse(self, response):
        links = response.css('ul#analysis-list-container>li')

        for link in links:
            try:
                article_url = link.css('a').attrib['href']
                yield scrapy.Request(response.urljoin(self.base_url + article_url), self.extract_article)
            except Exception as e:
                logging.basicConfig(filename='skip_urls.log', level=logging.DEBUG)

    def extract_article(self, response):
        headline = response.css('h1::text').extract_first()
        time = response.css('div.mc-info>time::text').extract_first()
        company = response.css('div.mc-info>a::text').extract_first()

        for i in response.css('div#bullets_ul>p>a'):
            if '/pr/' in i.attrib['href']:
                press_release = i.attrib['href']
                break
            elif '/filing/' in i.attrib['href']:
                press_release = i.attrib['href']
                break
            else:
                press_release = ''

        text = ''.join(response.css('div#bullets_ul>p::text').extract())

        row = {'headline': headline, 'time': time, 'company': company, 'url': response.url,
               'press_release_link': press_release,
               'text': text}

        yield row
